package lib

type DeviceState uint8

const (
	DEVICE_UP DeviceState = 1
	DEVICE_DOWN DeviceState = 2
)